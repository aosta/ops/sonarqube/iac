# ---------------------------------------------------------------------------------------------------------------------
# GLOBAL PARAMETERS
# These variables apply to all configurations in this subfolder. These are automatically merged into the child
# `terragrunt.hcl` config via the include block.
# ---------------------------------------------------------------------------------------------------------------------

# Configure root level variables that all resources can inherit. This is especially helpful with multi-account configs
# where terraform_remote_state data sources are placed directly into the modules.
inputs = merge(
  {
    tags = {
      Product     = "aosta"
      Environment = local.env
      Owner       = "Mick Pollard"
      Department  = "Product"
      Purpose     = "SAST with SonarQube"
      Region      = local.aws_region
      Terraform   = "true"
    }
  },
  local.account_vars.locals,
  local.environment_vars.locals,
)

## Nothing to change below this line

# Locals inherited from lower-level hcl files for reuse.

locals {
  account_vars     = read_terragrunt_config(find_in_parent_folders("account.hcl"))
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))

  account_name = local.account_vars.locals.account_name
  account_id   = local.account_vars.locals.aws_account_id
  aws_region   = local.account_vars.locals.aws_region
  env          = local.environment_vars.locals.env
  vpc_id       = local.environment_vars.locals.vpc_id
}

## Rely on AWS keys set by env vars

generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "aws" {
  region = "${local.aws_region}"
}
EOF
}

## Store Terraform state and locks in AWS

remote_state {
  backend = "s3"
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
  config = {
    bucket = "aosta-sonarqube-terragrunt-state"
    s3_bucket_tags = {
      Product     = "aosta"
      Environment = "Production"
      Owner       = "Mick Pollard"
      Department  = "Product"
      Purpose     = "Shared terraform state for https://gitlab.com/aosta/ops/sonarqube/iac"
      Region      = local.aws_region
      Terraform   = "true"
    }
    dynamodb_table_tags = {
      Product     = "aosta"
      Environment = "Production"
      Owner       = "Mick Pollard"
      Department  = "Product"
      Purpose     = "Shared terraform state for https://gitlab.com/aosta/ops/sonarqube/iac"
      Region      = local.aws_region
      Terraform   = "true"
    }
    key            = "${path_relative_to_include()}/terraform.tfstate"
    region         = local.aws_region
    encrypt        = true
    dynamodb_table = "aosta-sonarqube-terragrunt-lock-table"
  }
}
