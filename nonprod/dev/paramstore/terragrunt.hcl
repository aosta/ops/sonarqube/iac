# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.

terraform {
  source = "git::https://gitlab.com/aosta/ops/aws/ssm-parameter-store.git"
}

# These are the variables we have to pass in to use the module specified in the terraform source configuration above
#:FIXME: paramaterise the environemtn part of the path
inputs = {
  parameter_write = [
    {
      name        = "/aosta/sonarqube/dev/terraformgenerated/postgres_master_user"
      value       = dependency.db.outputs.this_rds_cluster_master_username
      type        = "String"
      overwrite   = "true"
      description = "aurora postgres master username"
    },
    {
      name        = "/aosta/sonarqube/dev/terraformgenerated/postgres_master_password"
      value       = dependency.db.outputs.this_rds_cluster_master_password
      type        = "SecureString"
      overwrite   = "true"
      description = "aurora postgres master password"
    },
    {
      name        = "/aosta/sonarqube/dev/terraformgenerated/aurora_postgres_db_name"
      value       = dependency.db.outputs.this_rds_cluster_database_name
      type        = "String"
      overwrite   = "true"
      description = "aurora postgres db name"
    },
    {
      name        = "/aosta/sonarqube/dev/terraformgenerated/aurora_postgres_endpoint"
      value       = dependency.db.outputs.this_rds_cluster_endpoint
      type        = "SecureString"
      overwrite   = "true"
      description = "aurora postgres endpoint"
    },
  ]
}

## Nothing to change below this line
dependency "db" {
  config_path = "../db"

  mock_outputs = {
    this_rds_cluster_database_name   = "db_name-mock"
    this_rds_cluster_endpoint        = "endpoint-mock"
    this_rds_cluster_master_password = "master_password-mock"
    this_rds_cluster_master_username = "master_username-mock"
  }
}

# Automatically load higher-level variables

locals {
  env_vars     = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  account_vars = read_terragrunt_config(find_in_parent_folders("account.hcl"))

  # Extract out common variables for reuse
  env          = local.env_vars.locals.env
  account_name = local.account_vars.locals.account_name
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}
