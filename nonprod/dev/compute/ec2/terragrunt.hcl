# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.

terraform {
  source = "git::https://gitlab.com/aosta/ops/aws/ec2.git"
}

# These are the variables we have to pass in to use the module specified in the terraform source configuration above

inputs = {
  name                   = "sonarqube-${local.account_name}-${local.env}"
  ami                    = "ami-aaaaa"
  instance_type          = "m5a.large"
  vpc_security_group_ids = [dependency.security_group.outputs.this_security_group_id]
  subnet_ids             = ["subnet-aaaaaaa"]
  key_name               = local.key_name
  iam_instance_profile   = "AmazonSSMReadOnlyAccess"
  user_data              = file("userdata.sh")
  lb_target_group_arn    = dependency.alb.outputs.target_group_arns
  lb_target_group_port   = 9000
  ebs_vol_id             = dependency.ebsvol.outputs.this_aws_ebs_vol_id
}

## Nothing to change below this line

# Dependecies from other configs

dependency "security_group" {
  config_path = "../../networking/security-groups"

  mock_outputs = {
    this_security_group_id = "security-group-id-mock"
  }
}

dependency "alb" {
  config_path = "../../networking/alb"

  mock_outputs = {
    target_group_arns = "target_group_arn-mock"
  }
}

dependency "ebsvol" {
  config_path = "../vol"

  mock_outputs = {
    this_aws_ebs_vol_id = "ebs_vol_id-mock"
  }
}

dependencies {
  paths = [
    "../../db",
    "../../paramstore",
    "../vol"
  ]
}

# Automatically load higher-level variables

locals {
  env_vars     = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  account_vars = read_terragrunt_config(find_in_parent_folders("account.hcl"))

  # Extract out common variables for reuse
  env          = local.env_vars.locals.env
  account_name = local.account_vars.locals.account_name
  key_name     = local.env_vars.locals.key_name
  aws_region   = local.account_vars.locals.aws_region
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}
