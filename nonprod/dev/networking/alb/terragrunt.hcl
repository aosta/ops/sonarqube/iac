# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.

terraform {
  source = "git::https://gitlab.com/aosta/ops/aws/alb.git"
}

# These are the variables we have to pass in to use the module specified in the terraform source configuration above

inputs = {
  name               = "${local.account_name}-${local.env}-sonarqube-nlb"
  vpc_id             = local.vpc_id
  subnets            = ["subnet-00000000000000000"]
  load_balancer_type = "network"
  internal           = true

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "TCP"
      target_group_index = 0
    }
  ]

  target_groups = [
    {
      name_prefix      = "tg-"
      backend_protocol = "TCP"
      backend_port     = 9000
      target_type      = "instance"
    }
  ]
}

## Nothing to change below this line
#
# Automatically load higher-level variables

locals {
  env_vars     = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  account_vars = read_terragrunt_config(find_in_parent_folders("account.hcl"))

  # Extract out common variables for reuse
  env          = local.env_vars.locals.env
  account_name = local.account_vars.locals.account_name
  vpc_id       = local.env_vars.locals.vpc_id
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}

