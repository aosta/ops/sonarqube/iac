# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.

terraform {
  source = "git::https://gitlab.com/aosta/ops/aws/route53-records.git"
}

# These are the variables we have to pass in to use the module specified in the terraform source configuration above

inputs = {
  name    = "sq"
  zone_id = "Z0000000000000000000"
  type    = "A"
  alias = {
    name                   = dependency.alb.outputs.this_lb_dns_name
    zone_id                = dependency.alb.outputs.this_lb_zone_id
    evaluate_target_health = false
  }
}

## Nothing to change below this line
# Dependecies from other configs

dependency "alb" {
  config_path = "../alb"

  mock_outputs = {
    this_lb_dns_name = "lb_name-mock"
    this_lb_zone_id  = "lb_zone_id-mock"
  }
}

# Automatically load higher-level variables

locals {
  env_vars     = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  account_vars = read_terragrunt_config(find_in_parent_folders("account.hcl"))

  # Extract out common variables for reuse
  env          = local.env_vars.locals.env
  account_name = local.account_vars.locals.account_name
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}
