# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.

terraform {
  source = "git::https://gitlab.com/aosta/ops/aws/rds-aurora.git"
}

# These are the variables we have to pass in to use the module specified in the terraform source configuration above

inputs = {
  name                    = "sonarqube-${local.account_name}-${local.env}"
  engine                  = "aurora-postgresql"
  engine_version          = "11.6"
  subnets                 = ["subnet-00000000000000000", "subnet-0000000000000000", "subnet-0000000000000000"]
  vpc_id                  = local.vpc_id
  allowed_security_groups = [dependency.security_group.outputs.this_security_group_id]
  replica_count           = 2
  instance_type           = "db.r4.large"
  apply_immediately       = true
  storage_encrypted       = false
  database_name           = "sonarqube"
}

## Nothing to change below this line

# Dependecies from other configs

dependency "security_group" {
  config_path = "../networking/security-groups"

  mock_outputs = {
    this_security_group_id = "security-group-id-mock"
  }
}

# Automatically load higher-level variables

locals {
  env_vars     = read_terragrunt_config(find_in_parent_folders("env.hcl"))
  account_vars = read_terragrunt_config(find_in_parent_folders("account.hcl"))

  # Extract out common variables for reuse
  env          = local.env_vars.locals.env
  account_name = local.account_vars.locals.account_name
  key_name     = local.env_vars.locals.key_name
  vpc_id       = local.env_vars.locals.vpc_id
}

# Include all settings from the root terragrunt.hcl file
include {
  path = find_in_parent_folders()
}
