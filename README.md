# SonarQube on AWS

This is the IaC for the install of SonarQube at Company X.
Owned and maintained as a best efforts service by Mick Pollard.

Non-Prod: http://sq.np.aosta.valueline.io **Coming Soon**
Prod: http://sq.aosta.valueline.io/ - **Coming Soon**

**This is currently a WIP**

SonarQube is built by [Packer](https://gitlab.com/aosta/ops/sonarqube/ami-factory) into an AMI and then is rolled out via [Terragrunt](https://gitlab.com/aosta/ops/sonarqube/iac).

## TODO

* document how to contribute / drive the automation
* Add TLS/ACM
* Add centralised logging
* improve the ability to tune SonarQube settings (ssm param store)
* migrate to ASG (1min:1max) for autohealing - if possible.
* Add observability + monitoring
* make it work from within CI
* Feedback loops (slack, gitlab MR's etc)
* add IAM roles/policies to terragrunt
